FROM ubuntu:16.04
MAINTAINER Ali Sadat <sadatakhavi.ali@gmail.com>

# Set environment variables.
ENV APP_NAME="Firefox"
ENV LANG en-US
ENV DEBIAN_FRONTEND noninteractive

COPY local.conf /etc/fonts/local.conf


# To avoide error during build   # https://github.com/dokku/dokku/issues/1542
# Set the locale
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen en_US en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
RUN dpkg-reconfigure locales
#RUN dokku config:set --global TZ=Europe/Berlin


# Update the system
RUN apt-get update \
  && apt-get install -y \
  apt-utils \
  wget \
                       libx11-dev libxtst-dev libxcomposite-dev \
                       libxdamage-dev libxkbfile-dev python-all-dev \
                       python-gobject-dev python-gtk2-dev cython \
                       xvfb xauth x11-xkb-utils libx264-dev libvpx-dev \
                       libswscale-dev libavcodec-dev subversion websockify curl \
  libcanberra-gtk-module libcanberra-gtk3-module \
                        libgtk-3-0 libgtk-3-common libcurl3 libnspr4 libnss3 libpango1.0 libcanberra-gtk3-module \
packagekit-gtk3-module libgl1-mesa-dri libvdpau-va-gl1 va-driver-all fonts-dejavu pulseaudio fonts-noto-cjk icedtea-netx \
  ca-certificates \
                        dbus-x11 ttf-ubuntu-font-family \
  --no-install-recommends
# \
#  && rm -rf /var/lib/apt/lists/*

#==============
# Install Xpra
#==============
RUN   apt-get install -y xpra
RUN   apt-get install -y xserver-xorg-video-dummy

ADD http://xpra.org/xorg.conf /home/xpra/xorg.conf
RUN /bin/echo -e "export DISPLAY=:100" > /home/xpra/.profile        # && chown xpra:xpra /home/xpra/xorg.conf


#============
# Firefox installation
#============
#============
# GeckoDriver 
#============

ARG GECKODRIVER_VERSION=0.21.0
RUN wget --no-verbose -O /tmp/geckodriver.tar.gz https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz \ 
  && rm -rf /opt/geckodriver \
  && tar -C /opt -zxf /tmp/geckodriver.tar.gz \
  && rm /tmp/geckodriver.tar.gz \
  && mv /opt/geckodriver /opt/geckodriver-$GECKODRIVER_VERSION \
  && chmod 755 /opt/geckodriver-$GECKODRIVER_VERSION \
  && ln -fs /opt/geckodriver-$GECKODRIVER_VERSION /usr/bin/geckodriver \
  && export PATH=$PATH:/path-to-extracted-file/geckodriver



RUN apt-get update \
  && apt-get install -y \
	dirmngr \
	gnupg \
	--no-install-recommends \
	&& apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0AB215679C571D1C8325275B9BDB3D89CE49EC21 \
	&& echo "deb http://ppa.launchpad.net/mozillateam/firefox-next/ubuntu xenial main" >> /etc/apt/sources.list.d/firefox.list \
	&& apt-get update \
  && apt-get install -y \
	ca-certificates \
	firefox \
	hicolor-icon-theme \
	libasound2 \
	libgl1-mesa-dri \
	libgl1-mesa-glx \
	--no-install-recommends \
  && rm -rf /var/lib/apt/lists/*


RUN apt-get update \
  && apt-get install -y  flashplugin-installer 




### https://github.com/sassmann/docker
#
##wget http://launchpadlibrarian.net/177041650/libfontconfig1-dev_2.11.0-0ubuntu4.1_amd64.deb
##sudo apt-get install libexpat1-dev
##sudo dpkg -i libfontconfig1-dev_2.11.0-0ubuntu4.1_amd64.deb
#
#RUN mkdir -p /etc/firefox/
#RUN echo -e '\
#pref("browser.crashReports.unsubmittedCheck.autoSubmit", true);\n\
#pref("browser.startup.homepage","data:text/plain,browser.startup.homepage=https://www.startpage.com");\n\
#pref("datareporting.healthreport.service.firstRun", false);\n\
#pref("datareporting.healthreport.uploadEnabled", false);\n\
#pref("datareporting.policy.dataSubmissionPolicyBypassNotification", true);\n\
#pref("reader.parse-on-load.enabled", false);\n\
#pref("signon.rememberSignons", false);\n\
#pref("toolkit.telemetry.reportingpolicy.firstRun", false);\n'\
#>> /etc/firefox/syspref.js
##FIXME: does not stick
##pref("browser.search.widget.inNavBar", true);\n\


# expose raw TCP port
EXPOSE 10000

# Run! Application will be served both as WebSocket and Raw TCP. (+ disable all unsupported bits)
CMD ["xpra", "start", ":100", "--start-child=firefox",  "--daemon=off", "--bind-tcp=0.0.0.0:10000", "--no-mdns", "--no-notifications", "--no-pulseaudio"]
#CMD ["xpra", "start", ":100", "--start-child=firefox",  "--daemon=off", "--bind-tcp=0.0.0.0:10000", "--no-mdns", "--no-notifications"]

